package cz.wordcount;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Activated clown vision and everything looked exactly the same.
 */
public class ReturnToMonke {

	public static Set<BitSet> wordCount = ConcurrentHashMap.newKeySet();
	public static void countWords(byte[] arr) {
		int wordStart = 0;

		for (int i = 0; i < arr.length; i++) {
			// 1 byte char
			if ((arr[i] & 0b10000000) == 0) {
				switch (arr[i]) {
					case '\u0009':
					case '\n':
					case '\u000B':
					case '\u000C':
					case '\r':
					case ' ':
						if (i != wordStart) {
							wordCount.add(BitSet.valueOf(Arrays.copyOfRange(arr, wordStart, i)));
						}
						wordStart = i + 1;
				}
			} else {
				switch (arr[i] & 0b11110000) {
					// 2 byte char
					//case 0b11000000: // no hits
					case 0b11010000:
						// 2 possibilities nextLine/no-break space
						if ((arr[i] == (byte) 0b11010000 && arr[i + 1] == (byte) 0b10101000) || (arr[i] == (byte) 0b11010100 && arr[i + 1] == (byte) 0b10000000)) {
							if (i != wordStart) {
								wordCount.add(BitSet.valueOf(Arrays.copyOfRange(arr, wordStart, i)));
							}
							wordStart = i + 2;
						}
						++i; // move 1 byte to right to get to end of double byte

					// 3 byte char
					case 0b11100000:
						if ((arr[i] == (byte) 0b11100001 && arr[i + 1] == (byte) 0b10011010 && arr[i + 2] == (byte) 0b10000000) // U+1680
								|| (arr[i] == (byte) 0b11100010 && arr[i + 1] == (byte) 0b10000000  // U+2000 - U+200A
								&& (arr[i + 2] == (byte) 0b10000000 || arr[i + 2] == (byte) 0b10000001 || arr[i + 2] == (byte) 0b10000010
								|| arr[i + 2] == (byte) 0b10000011 || arr[i + 2] == (byte) 0b10000100 || arr[i + 2] == (byte) 0b10000101
								|| arr[i + 2] == (byte) 0b10000110 || arr[i + 2] == (byte) 0b10000111 || arr[i + 2] == (byte) 0b10001000
								|| arr[i + 2] == (byte) 0b10001001 || arr[i + 2] == (byte) 0b10001010))
								|| (arr[i] == (byte) 0b11100010
								&& ((arr[i + 1] == (byte) 0b10000000
								&& (arr[i + 2] == (byte) 0b10101000 || arr[i + 2] == (byte) 0b10101001 || arr[i + 2] == (byte) 0b10101111)) // U+1680 + U+1681 + U+202F
								|| (arr[i + 1] == (byte) 0b10000001 && arr[i + 2] == (byte) 0b10011111))) // U+205F
								|| (arr[i] == (byte) 0b11100011 && arr[i + 1] == (byte) 0b10000000 && arr[i + 2] == (byte) 0b10000000)) {

							if (i != wordStart) {
								wordCount.add(BitSet.valueOf(Arrays.copyOfRange(arr, wordStart, i)));
							}
							wordStart = i + 3;
						}
						i += 2; // move 2 bytes to right to get to end of triple byte
						break;
					default:
				}
			}
		}
		if (wordStart < arr.length) {
			wordCount.add(BitSet.valueOf(Arrays.copyOfRange(arr, wordStart, arr.length)));
		}
	}
}
